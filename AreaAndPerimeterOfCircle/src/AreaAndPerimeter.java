
public class AreaAndPerimeter {

	public static void main(String[] args) {
		
		//Program that displays the area and the perimeter
		
		final double radius = 5.5;
		final double pi = 3.14245;
		
		double perimeter = 2 * radius * pi;
        double area = radius * radius * pi;

        System.out.println(" The Perimeter of the Circle is: " + perimeter );
        System.out.println(" The Area of the circle is:" + area);
		
	}

}
