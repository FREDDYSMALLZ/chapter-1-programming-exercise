
public class AreaAndPerimeter {

	public static void main(String[] args) {
		
		final double radius = 5.5;
		
		double Perimeter = 2 *  Math.PI;
		double Area = radius * radius * Math.PI;
		
		System.out.println(" The perimeter of the circle is: " + Perimeter);
		System.out.println(" The Area of the circle is: " + Area);
		
		
      
		
	}

}
