
public class AvarageSpeed {

	public static void main(String[] args) {
		// Average speed of a runner
		// 1 mile is equal to 1.6 Kilometers
		
		double Kilometers = 14.0;
		double Miles = Kilometers / 1.6;  //Distance in miles 
		double Time = 45.50 / 60;        //Time in hours
		double AverageSpeed = Miles / Time;
		
		System.out.println(" The Average speed of the runner is: " 
		+ AverageSpeed + " MilesPerHour ");
		
		
		
		
		
		
	}

}
