
public class PopulationProjection {

	public static void main(String[] args) {
	// One Birth  every 7 seconds
	// One Death every 10 seconds
	// One new Immigrant every 45 seconds
	// current population is 312,032,486
		
		double BirthRateInSeconds = 7.0;
		double DeathRateInSeconds = 10.0;
		double NewImmigrantInSeconds = 45.0;
		double CurrentPopulation = 312032486;
		double SecondsInYears = 60 * 60 * 24 * 365; 
		
		double numberOfBirths = SecondsInYears / BirthRateInSeconds;
		double numberOfDeaths = SecondsInYears / DeathRateInSeconds;
		double numberOfNewImmigrants = SecondsInYears / NewImmigrantInSeconds;
		
		
		for (int i = 1; i <= 5; i++) {
            CurrentPopulation += numberOfBirths + numberOfNewImmigrants - numberOfDeaths;
            System.out.println("Year " + i + " = " + (int)CurrentPopulation);

        }


		
		
	}

}
