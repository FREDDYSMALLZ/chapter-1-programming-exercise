
public class AverageSpeed {

	public static void main(String[] args) {
		double Hours = 1.0;
		double Minutes = 45.0;
		double Seconds = 35.0;
		
		// Average speed in  Kilometers per hour
		//1 mile is equal to 1.6 kilometers
		double DistanceInMiles = 24.0;
		double DistanceInKilometers = DistanceInMiles * 1.6;
		double TotalTimeTaken = Hours * 60 + Minutes + (Seconds/60);
		
		System.out.println("The Average Speed of the runner is: " 
		+ (DistanceInKilometers / TotalTimeTaken) * 60 + " Kph ");
		
		
		
		
	}

}
